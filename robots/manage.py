import click
import logic.voyager as voyager


@click.group()
def main():
    pass


@main.command()
def run():
    voyager.Voyager.run()


if __name__ == "__main__":
    main()
