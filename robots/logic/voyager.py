import time
import random
import requests
import config
import common.loggers as loggers
import logic.helpers as helpers
import logic.roboticfactory as roboticfactory


class Voyager:

    _CONFIG = config.Config.get()
    _logger = loggers.Loggers.get_robots_logger()

    @staticmethod
    def run():

        robot = roboticfactory.RoboticFactory.build("generic")

        while True:

            response = requests.get(Voyager._CONFIG.UPLOADER_READ_ROUTE_URL)

            if response.status_code != requests.codes.ok:
                sleep_time = random.randint(0, 60)
                Voyager._logger.info(f"No routes to execute, sleeping for {sleep_time} second(s).")
                time.sleep(sleep_time)
                continue

            route = helpers.RoboticRouteAdapter(response.json())
            Voyager._logger.info("----------- Received a robotic route for execution -----------")
            robot.execute_route(route)
            Voyager._logger.info("----------- COMPLETED -----------\n")
