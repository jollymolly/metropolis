class RoboticRouteAdapter:

    def __init__(self, robotic_route_data):
        self.__robotic_route_data = robotic_route_data

    @property
    def start_coordinates(self):
        return self.__robotic_route_data["start_coordinates"]

    @property
    def instructions(self):
        return self.__robotic_route_data["instructions"]
