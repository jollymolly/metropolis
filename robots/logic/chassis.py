import abc


class IChassis(metaclass=abc.ABCMeta):

    def __init__(self):
        self.__brain = None

    @property
    def brain(self):
        return self.__brain

    @brain.setter
    def brain(self, brain):
        self.__brain = brain

    @abc.abstractmethod
    def turn_left(self):
        pass

    @abc.abstractmethod
    def turn_right(self):
        pass

    @abc.abstractmethod
    def move_delta(self, dx, dy):
        pass

    def execute_route(self, route):
        self.brain.set_coordinates(*route.start_coordinates)
        self.brain.set_direction()

        self.brain.report_position()

        for instruction in route.instructions:
            self.brain.interpret(instruction)

        self.brain.report_position()


class GenericChassis(IChassis):

    def turn_left(self):
        pass

    def turn_right(self):
        pass

    def move_delta(self, dx, dy):
        pass
