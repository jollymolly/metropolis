import abc
import config
import random
import common.loggers as loggers

_logger = loggers.Loggers.get_robots_logger()


class IBrain(metaclass=abc.ABCMeta):
    """ feels like there is a more efficient way to implement calculations - extract into some calculation class
        using slots and leave more stripped iface for Brain(s) - but not for today, should be revisited on a clear head.
    """

    def __init__(self):
        self.__commandable = None
        self.__x, self.__y = None, None
        self.__direction = None

    @property
    def commandable(self):
        return self.__commandable

    @commandable.setter
    def commandable(self, commandable):
        self.__commandable = commandable

    @property
    def x(self):
        return self.__x

    @x.setter
    def x(self, value):
        self.__x = value

    @property
    def y(self):
        return self.__y

    @y.setter
    def y(self, value):
        self.__y = value

    @property
    def direction(self):
        return self.__direction

    @direction.setter
    def direction(self, value):
        self.__direction = value

    def report_position(self):
        _logger.info(f"Located at ({self.x}, {self.y}) directed {self.direction}.")

    @abc.abstractmethod
    def set_coordinates(self, x, y):
        pass

    @abc.abstractmethod
    def set_direction(self, direction=None):
        pass

    @abc.abstractmethod
    def interpret(self, instruction):
        pass


class GenericBrain(IBrain):

    _DIRECTIONS = config.Config.get().GO_DIRECTIONS  # redefine explicitly one more time might be a better idea
    _DIRECTIONS_LOOKUP = frozenset(_DIRECTIONS)
    _TURN_COMMAND = "t"
    _GO_COMMAND = "g"

    _TURNS_AT_CURRENT_DIRECTION_MAP = {
        # -1 = turn left, 1 = turn right
        _DIRECTIONS.west: {-1: _DIRECTIONS.south, 1: _DIRECTIONS.north},
        _DIRECTIONS.north: {-1: _DIRECTIONS.west, 1: _DIRECTIONS.east},
        _DIRECTIONS.east: {-1: _DIRECTIONS.north, 1: _DIRECTIONS.south},
        _DIRECTIONS.south: {-1: _DIRECTIONS.east, 1: _DIRECTIONS.west}
    }

    _TURNS_BETWEEN_ANY_DIRECTIONS_MAP = {
        _DIRECTIONS.west: {_DIRECTIONS.north: (1, ), _DIRECTIONS.east: (1, 1, ), _DIRECTIONS.south: (-1, )},
        _DIRECTIONS.north: {_DIRECTIONS.east: (1, ), _DIRECTIONS.south: (1, 1, ), _DIRECTIONS.west: (-1, )},
        _DIRECTIONS.east: {_DIRECTIONS.south: (1, ), _DIRECTIONS.west: (1, 1, ), _DIRECTIONS.north: (-1, )},
        _DIRECTIONS.south: {_DIRECTIONS.west: (1, ), _DIRECTIONS.north: (1, 1, ), _DIRECTIONS.east: (-1, )}
    }

    def __init__(self, verbal_module=None):
        super().__init__()

    def set_coordinates(self, x, y):
        self.x, self.y = x, y
        return self.x, self.y

    def set_direction(self, direction=None):
        self.direction = random.choice((self._DIRECTIONS.north, self._DIRECTIONS.east))

    def interpret(self, instruction):

        _logger.info(f"Input instruction: \"{instruction}\"")

        what, where = instruction.split()

        if what == self._TURN_COMMAND:
            self.turn(int(where))
        elif what == self._GO_COMMAND:
            self.go(where)
        else:
            raise RuntimeError("Brain no more.")

    def turn(self, where):

        original_direction = self.direction

        command = self.commandable.turn_left
        if where > 0:
            command = self.commandable.turn_right

        where //= abs(where)  # in case where is more then 1 turn, make it 1 turn preserving if it is left(-) or right

        for i in range(0, abs(where)):
            command()
            self.direction = self._TURNS_AT_CURRENT_DIRECTION_MAP[self.direction][where]

        _logger.info(f"Made turn to the {'left' if where < 0 else 'right'}, located at ({self.x}, {self.y}),"
                     f"direction change: {original_direction}->{self.direction}.")

    def go(self, where):

        distance, direction = None, None

        if where[-1] in self._DIRECTIONS_LOOKUP and where[-2].isdigit():
            distance, direction = int(where[:-1]), where[-1]  # do not validate direction itself, trust for now
        else:
            try:
                distance = int(where)
            except Exception:
                distance = where  # it's a landmark, distance should be requested from city map, but no support yet

        if direction is not None and self.direction != direction:
            where = sum(self._TURNS_BETWEEN_ANY_DIRECTIONS_MAP[self.direction][direction])
            self.turn(where)
        else:
            direction = self.direction

        new_x, new_y = self.x, self.y

        if direction == self._DIRECTIONS.east:
            new_x += distance
        elif direction == self._DIRECTIONS.west:
            new_x += -1 * distance
        elif direction == self._DIRECTIONS.north:
            new_y += distance
        else:
            new_y += -1 * distance

        if new_x < 0 or new_y < 0:
            _logger.info(
                f"Calculated move is beyond city's grid ({new_x}, {new_y}), skipping. Located at ({self.x}, {self.y}"
            )
        else:
            dx, dy = new_x - self.x, new_y - self.y
            self.commandable.move_delta(dx, dy)
            self.x, self.y = new_x, new_y
            _logger.info(f"Moved to a location ({self.x}, {self.y}), coordinates deltas: ({dx}, {dy})")
