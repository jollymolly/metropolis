import logic.brains as brains
import logic.chassis as chassis


class RoboticFactory:

    @staticmethod
    def build(model):

        robot = None

        if model == "generic":
            robot = RoboticFactory.build_generic_robot()
        else:
            raise TypeError(f"Unsupported robot model \"{model}\"")

        return robot

    @staticmethod
    def build_generic_robot():
        commandable, brain = chassis.GenericChassis(), brains.GenericBrain()
        commandable.brain, brain.commandable = brain, commandable
        return commandable
