import unittest

import logic.brains as brains


class BrainsTestCase(unittest.TestCase):

    def test_ibrains_abs_raises_on_instantination(self):

        self.assertRaises(TypeError, brains.IBrain)
