import os
import flask
import flask_restful


class App:
    _app_name = os.path.basename(os.path.dirname(__file__))
    _app = flask.Flask(_app_name)

    @classmethod
    def get(cls):
        api = Api.get(cls._app_name)
        api.init_app(cls._app)
        return cls._app


class Api:
    _apis = dict()

    @classmethod
    def get(cls, prefix, version="v1"):
        if version not in cls._apis:

            api = flask_restful.Api(prefix=f"/{prefix}/{version}")

            if version == "v1":
                cls._register_v1(api)
            else:
                raise RuntimeError(f"Unsupported api version \"{version}\" has been requested.")

            cls._apis[version] = api

        return cls._apis[version]

    @classmethod
    def _register_v1(cls, api):

        import logic.resources as resources

        api.add_resource(resources.DatabaseLoadResource, "/database/load/<uuid:input_route_uuid>", strict_slashes=False)
        api.add_resource(resources.DatabaseReadResource, "/database/read", strict_slashes=False)

        return api
