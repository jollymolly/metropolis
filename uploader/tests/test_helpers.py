import unittest

import logic.helpers as helpers


class HelpersTestCase(unittest.TestCase):

    def test_convert_to_robotic(self):

        input_output_instructions_map = {
            "turn left": "t -1",
            "turn right": "t 1",
            "go distance 3 in direction N": "g 3N",
            "go distance 5": "g 5",
            "go until you reach a landmark": "g landmark"
        }

        for input, output in input_output_instructions_map.items():
            self.assertEqual(helpers.InputRouteAdapter._convert_to_robotic(input), output)
