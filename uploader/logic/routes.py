import config
import common.database.connector as database_connector
import common.database.models as models
import logic.helpers as helpers


class InputToRoboticRouteConverter:

    _CONFIG = config.Config.get()

    @staticmethod
    def store(input_route_uuid):
        db_connector = database_connector.DataBaseConnector.get()

        robotic_route_ret = dict()

        with db_connector.get_session() as session:
            in_route = session.query(models.InputRoutes)\
                .filter(models.InputRoutes.input_route_uuid == input_route_uuid)\
                .with_for_update()\
                .one()

            if in_route is not None \
                    and in_route.status != InputToRoboticRouteConverter._CONFIG.INPUT_ROUTES_STATUS_VALUES[
                    InputToRoboticRouteConverter._CONFIG.INPUT_ROUTES_STATUS_ARCHIVE]:

                in_route_adapter = helpers.InputRouteAdapter(in_route)

                robotic_route_uuid = config.Config.get().uuid_factory()

                robotic_route = models.RoboticRoutes(
                    robotic_route_uuid=robotic_route_uuid,
                    start_coordinates=in_route_adapter.get_start_coordinates()
                )

                robotic_route.instructions.append(
                    models.RoboticRoutesInstructions(
                        robotic_route_uuid=robotic_route_uuid,
                        instructions=in_route_adapter.get_instructions()
                    )
                )

                session.add(robotic_route)

                input_route_status = InputToRoboticRouteConverter._CONFIG.INPUT_ROUTES_STATUS_VALUES[
                    InputToRoboticRouteConverter._CONFIG.INPUT_ROUTES_STATUS_ARCHIVE]
                session.query(models.InputRoutes).filter(models.InputRoutes.input_route_uuid == input_route_uuid)\
                    .update({models.InputRoutes.status: input_route_status})
                robotic_route_ret = robotic_route.to_dict()

        return robotic_route_ret


class RoboticRoutesReader:

    _CONFIG = config.Config.get()
    _STATUS_NEW = _CONFIG.ROBOTIC_ROUTES_STATUS_VALUES[_CONFIG.ROBOTIC_ROUTES_STATUS_NEW]
    _STATUS_TAKEN = _CONFIG.ROBOTIC_ROUTES_STATUS_VALUES[_CONFIG.ROBOTIC_ROUTES_STATUS_TAKEN]

    @staticmethod
    def read():
        db_connector = database_connector.DataBaseConnector.get()

        robotic_route_dict = dict()

        with db_connector.get_session() as session:

            robotic_route = session.query(models.RoboticRoutes)\
                .filter(models.RoboticRoutes.status == RoboticRoutesReader._STATUS_NEW)\
                .order_by(models.RoboticRoutes.created_at)\
                .with_for_update()\
                .first()

            if robotic_route is not None:
                robotic_route_dict = robotic_route.to_dict()
                robotic_route_dict["instructions"] = list()
                for robotic_route_instructions in robotic_route.instructions:
                    robotic_route_dict["instructions"] += robotic_route_instructions.to_dict()["instructions"]

                robotic_route.status = RoboticRoutesReader._STATUS_TAKEN
                session.merge(robotic_route)

        return robotic_route_dict
