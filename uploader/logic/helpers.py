import re


class InputRouteAdapter:
    """Convert custom instructions sent by planner to generic instructions form used by robots"""

    _COORDINATES_RE = re.compile(r"\(\s*(\d+),\s*(\d+)\s*\)$")

    def __init__(self, input_route_model):
        self.__input_route_model = input_route_model

    @property
    def model(self):
        return self.__input_route_model

    @staticmethod
    def _convert_to_robotic(instruction):
        """
        assume there is always only the following instructions as input (validation has to have been made outside):
            turn left -> t -1
            turn right -> t 1
            go distance x in direction N/S/E/W -> g x[NSEW]?
            go distance x -> g x
            go until you reach a landmark -> g landmark
        """

        # looks like it could be done using wisely constructed RE, but skipping it after spending some time in debug
        if instruction.startswith("turn"):
            what = "t"
            where = -1 if instruction.split()[1] == "left" else 1
        else:
            what = "g"
            split_instr = instruction.split()
            if split_instr[1] == "until":
                where = split_instr[-1]
            elif len(split_instr) == 3:
                where = split_instr[2]
            else:
                where = f"{split_instr[2]}{split_instr[-1]}"

        return f"{what} {where}"

    def get_start_coordinates(self):
        x, y = self._COORDINATES_RE.search(self.model.definition["start_coordinates"]).groups()
        return int(x), int(y)

    def get_instructions(self):
        return list(map(InputRouteAdapter._convert_to_robotic, self.model.definition["instructions"]))
