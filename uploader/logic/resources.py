import flask
import flask_restful
import logic.routes as routes_logic


class DatabaseLoadResource(flask_restful.Resource):

    def post(self, input_route_uuid):
        input_route_uuid = str(input_route_uuid)

        robotic_route = routes_logic.InputToRoboticRouteConverter.store(input_route_uuid)

        response = flask.jsonify(robotic_route)
        response.status = "200" if robotic_route else "500"

        return response


class DatabaseReadResource(flask_restful.Resource):

    def get(self):

        robotic_route_instructions = routes_logic.RoboticRoutesReader.read()

        # TODO: remove duplicated code (although it might beoome different with time) within base resource for both
        response = flask.jsonify(robotic_route_instructions)
        response.status = "200" if robotic_route_instructions else "404"

        return response

    # lacking put method to mark robotic route as archive, maybe will do if there are some time left
