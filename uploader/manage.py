import click
import app


@click.group()
def main():
    pass


@main.command()
def run():
    app.App.get().run()  # TODO: run behind real server


if __name__ == "__main__":
    main()
