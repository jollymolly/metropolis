BRIEF DESCRIPTION

	- There are three applications (planner, uploader, robots) as specified in technical document;
	- common directory contains common logic used by all applications but requires 
	  PYTHONPATH=../metropolis to be exported everywhere;
	- uploader is a primitive REST Api to upload/read routes and should be started first, because 
	  it is used by the remaining two - planner (to upload routes) and robot (to read ones);
	- There is one more - service - application to support database/tables create/drop;
	- Once all three are started:

		* planner starts to generate plans (some finite number of them controlled through config)
		  and store them through uploader;
		* robots starts to read available plans execute them and waits if there are no more
		  available;
		* uploader stores planner's plans into db and serves robot with ones to execute,
                  read plan is marked as 'taken' and won't be served to robot again.

RUNNING INSTRUCTIONS

	- Postgresql running in a background with 'postgres:password' creds is a prerequisite;
	- There is a makefile the supports database drop/create and create virtualenv through command:
	
		make clean && make setup

	- There is a 'make run' command to start all three applications, but it is far from perfect
	  and, although all three work as expected, terminal output is a mess;
	- Instructions to start from three different terminals:

		cd <somewhere>/metropolis
		export PYTHONPATHT=$(pwd)
		./venv/bin/python uploader/manage.py run
		sleep 2  # to let flask server become available
                ./venv/bin/python planner/manage.py run
                ./venv/bin/python robots/manage.py run


NOTES ON QUESTION WITHIN TECHNIAL DOC:

	- A system with million of routes vs. a system with < 100 routes
        - A system where routes have thousands of instructions each vs. a system where
	  routes have 1-10 instructions

		* database models might be revisited for batched instructions storing;
		* each step - planner/uploader/robots - have to be parallelized;

	- A system with millions of simultaneous users vs. <10 simultaneous users:

		* as previously - adding parallelizm will become neccessary;
		* database locks might require a review (but current schema should be ok
		  because of it's simplicity and locks already in-place);

	- A system where routes are frequently changed and updated vs. one where
   	  routes are permanent once initially devised:

		* route refresh (notification about change) once execution has been already
  	          started will be required (schema will require at least updated_at field
                  to be able to identify changed records).             
                 
