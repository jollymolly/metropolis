import logging


class Loggers:

    _PLANNER_LOGGER = "planner_logger"
    _UPLOADER_LOGGER = "uploader_logger"
    _ROBOTS_LOGGER = "robots_logger"

    _loggers = set()

    @staticmethod
    def _setup_named_logger(name):
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        logger.addHandler(logging.StreamHandler())
        Loggers._loggers.add(name)
        return logger

    @staticmethod
    def get_planner_logger():
        if Loggers._PLANNER_LOGGER not in Loggers._loggers:
            Loggers._setup_named_logger(Loggers._PLANNER_LOGGER)
        return logging.getLogger(Loggers._PLANNER_LOGGER)

    @staticmethod
    def get_uploader_logger():
        if Loggers._UPLOADER_LOGGER not in Loggers._loggers:
            Loggers._setup_named_logger(Loggers._UPLOADER_LOGGER)
        return logging.getLogger(Loggers._UPLOADER_LOGGER)

    @staticmethod
    def get_robots_logger():
        if Loggers._ROBOTS_LOGGER not in Loggers._loggers:
            Loggers._setup_named_logger(Loggers._ROBOTS_LOGGER)
        return logging.getLogger(Loggers._ROBOTS_LOGGER)
