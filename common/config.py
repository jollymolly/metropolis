import uuid
import collections


class Config:
    _DIRECTIONS_TYPE = collections.namedtuple("Dierctions", ("west", "north", "south", "east"))

    DATABASE_NAME = "metropolis"  # TODO remove hardcodes at least to the environment variables
    COMMON_DATABASE_URL = "postgresql://postgres:password@localhost:5432"
    DATABASE_URL = f"{COMMON_DATABASE_URL}/{DATABASE_NAME}"
    GO_DIRECTIONS = _DIRECTIONS_TYPE("W", "N", "S", "E")

    INPUT_ROUTES_STATUS_NEW = 0
    INPUT_ROUTES_STATUS_ARCHIVE = 1
    INPUT_ROUTES_STATUS_VALUES = ("NEW", "ARCHIVE")
    ROBOTIC_ROUTES_STATUS_NEW = INPUT_ROUTES_STATUS_NEW
    ROBOTIC_ROUTES_STATUS_ARCHIVE = INPUT_ROUTES_STATUS_ARCHIVE
    ROBOTIC_ROUTES_STATUS_TAKEN = 2
    ROBOTIC_ROUTES_STATUS_VALUES = INPUT_ROUTES_STATUS_VALUES + ("TAKEN", )

    UPLOADER_HOST = "localhost"
    UPLOADER_PORT = "5000"
    UPLOADER_LOCATION = f"http://{UPLOADER_HOST}:{UPLOADER_PORT}"
    UPLOADER_PREFIX = "uploader/v1"

    UPLOADER_ADD_INPUT_ROUTE_URL = "/".join((UPLOADER_LOCATION, UPLOADER_PREFIX, "database/load", "{input_route_uuid}"))
    UPLOADER_READ_ROUTE_URL = "/".join((UPLOADER_LOCATION, UPLOADER_PREFIX, "database/read"))

    @classmethod
    def get(cls):
        if "_instance" not in cls.__dict__:  # limit search scope to current cls object
            cls._instance = cls()
        return cls._instance

    @staticmethod
    def uuid_factory():
        return str(uuid.uuid4())
