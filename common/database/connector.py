import contextlib

import sqlalchemy
import common.config as common_config
import common.database.models as models

import logging


class DataBaseConnector:

    def __init__(self, database_url):
        self.__database_url = database_url
        self.__engine = None
        self.__sessionmaker = sqlalchemy.orm.session.sessionmaker()
        self.__sessionmaker.configure(bind=self.engine)

    @classmethod
    def get(cls, database_url=common_config.Config.get().DATABASE_URL):
        if not hasattr(cls, "_instance"):
            cls._instance = cls(database_url)
        return cls._instance  # if used with multiple threads - guard create_engine

    @property
    def engine(self):
        if self.__engine is None:
            self.__engine = sqlalchemy.create_engine(self.__database_url)
            # TODO: check received engine for operability
        return self.__engine

    @property
    def session(self):
        return self.__sessionmaker()  # scoped_session?

    @contextlib.contextmanager
    def get_session(self):
        session = self.session
        try:
            yield session
        except Exception:
            session.rollback()
            raise
        else:
            session.commit()
        session.close()

    @staticmethod
    def create_database():
        db_connector = DataBaseConnector.get(common_config.Config.get().COMMON_DATABASE_URL)
        connection = db_connector.engine.connect()
        connection.execution_options(isolation_level="AUTOCOMMIT")
        stmt = f"CREATE DATABASE {common_config.Config.get().DATABASE_NAME}"
        logging.warning(f"Going to create database: \"{stmt}\"")
        connection.execute(stmt)
        db_connector.engine.dispose()
        logging.warning(f"Database \"{common_config.Config.get().DATABASE_NAME}\" has been created.")

    @staticmethod
    def drop_database():
        db_connector = DataBaseConnector.get(common_config.Config.get().COMMON_DATABASE_URL)
        connection = db_connector.engine.connect()
        connection.execution_options(isolation_level="AUTOCOMMIT")
        stmt = f"DROP DATABASE {common_config.Config.get().DATABASE_NAME}"
        logging.warning(f"Going to drop database: \"{stmt}\"")
        connection.execute(stmt)
        db_connector.engine.dispose()
        logging.warning(f"Database \"{common_config.Config.get().DATABASE_NAME}\" has been removed.")

    @staticmethod
    def create_tables():
        db_connector = DataBaseConnector.get(common_config.Config.get().DATABASE_URL)
        with db_connector.engine.begin() as c:
            models.metadata.create_all(c)
        db_connector.engine.dispose()
        logging.warning(f"Database tables have been created.")
