import datetime
import sqlalchemy
import sqlalchemy.dialects.postgresql as postgresql
import sqlalchemy.ext.declarative as declarative

import common.config as config

metadata = sqlalchemy.MetaData()
_Base = declarative.declarative_base(metadata=metadata)

_CONFIG = config.Config.get()


class BaseModel(_Base):
    __abstract__ = True


class InputRoutes(BaseModel):
    __tablename__ = "input_routes"

    input_route_uuid = sqlalchemy.Column(postgresql.TEXT, primary_key=True)
    status = sqlalchemy.Column(
        postgresql.TEXT, default=_CONFIG.INPUT_ROUTES_STATUS_VALUES[_CONFIG.INPUT_ROUTES_STATUS_NEW]
    )
    definition = sqlalchemy.Column(postgresql.JSON)
    created_ts = sqlalchemy.Column(postgresql.TIMESTAMP, default=datetime.datetime.now)


class RoboticRoutes(BaseModel):
    __tablename__ = "robotic_routes"

    robotic_route_uuid = sqlalchemy.Column(postgresql.TEXT, primary_key=True)
    status = sqlalchemy.Column(
        postgresql.TEXT, default=_CONFIG.ROBOTIC_ROUTES_STATUS_VALUES[_CONFIG.ROBOTIC_ROUTES_STATUS_NEW]
    )
    start_coordinates = sqlalchemy.Column(postgresql.ARRAY(postgresql.INTEGER))
    created_at = sqlalchemy.Column(postgresql.TIMESTAMP, default=datetime.datetime.now)
    instructions = sqlalchemy.orm.relationship(
        "RoboticRoutesInstructions", backref="robotic_routes_instructions", cascade="all"
    )

    def to_dict(self):
        """simplified model to dict conversion, defined at basemodel level with inspect should be used instead"""

        return dict(
            robotic_route_uuid=self.robotic_route_uuid,
            status=self.status,
            start_coordinates=self.start_coordinates,
            created_at=self.created_at
        )


class RoboticRoutesInstructions(BaseModel):
    __tablename__ = "robotic_routes_instructions"

    robotic_route_instruction_uuid = sqlalchemy.Column(
        postgresql.TEXT, primary_key=True, default=config.Config.get().uuid_factory
    )
    robotic_route_uuid = sqlalchemy.Column(postgresql.TEXT, sqlalchemy.ForeignKey("robotic_routes.robotic_route_uuid"))
    instructions = sqlalchemy.Column(postgresql.JSON)

    def to_dict(self):
        """second defenition of exact same to_dict conversion logic - another reason to move into basemodel"""

        return dict(
            instructions=self.instructions
        )
