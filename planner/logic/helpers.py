import itertools
import random
import config

_CONFIG = config.Config.get()


class Instructions:

    TURNS = ("left", "right", )

    GO_WHERE_YOU_RE_GOING = ""
    GO_UNTIL_LANDMARK = "landmark"
    GO_IN_DIRECTION = "in direction"
    GO_TEMPLATE = "go {}"

    GOS = (GO_WHERE_YOU_RE_GOING, GO_IN_DIRECTION, )  # no reliable way to determine reachable landmarks, so not used

    @staticmethod
    def start():
        x = random.randint(0, _CONFIG.MAX_X)
        y = random.randint(0, _CONFIG.MAX_Y)
        return f"start at ({x}, {y})"

    @staticmethod
    def turn():
        where = random.choice(Instructions.TURNS)
        return f"turn {where}"

    @staticmethod
    def get_landmark():
        """Not actually used as it is a task of itself to determine which landmarks are within reach when autogen"""
        return random.choice(("landmark 1", "landmark 2", ))

    @staticmethod
    def go():

        go_type = random.choice(Instructions.GOS)

        ret_val = ""

        if go_type in frozenset((Instructions.GO_WHERE_YOU_RE_GOING, Instructions.GO_IN_DIRECTION)):
            blocks, direction = random.randint(1, _CONFIG.MAX_BLOCKS_AT_ONCE), ""
            ret_val = Instructions.GO_TEMPLATE.format(f"distance {blocks}")
            if go_type == Instructions.GO_IN_DIRECTION:
                direction = random.choice(_CONFIG.GO_DIRECTIONS)
                ret_val = "{ret_val} {direction}".format(ret_val=ret_val, direction=f"in direction {direction}")
        elif go_type == Instructions.GO_UNTIL_LANDMARK:
            ret_val = Instructions.GO_TEMPLATE.format("until you reach a landmark")
            ret_val = "{ret_val} \"{landmark}\"".format(ret_val=ret_val, landmark=Instructions.get_landmark())
        else:
            raise RuntimeError("Unsupported type of direcation to go has been chosen.")

        return ret_val


class RouteInstructionsGenerator:

    INSTRUCTIONS = (Instructions.turn, Instructions.go)

    def __iter__(self):
        yield Instructions.start()  # return starting position
        mapper = itertools.repeat(
            RouteInstructionsGenerator.INSTRUCTIONS,
            random.randint(_CONFIG.MIN_INSTRUCTIONS_PER_PLAN, _CONFIG.MAX_INSTRUCTIONS_PER_PLAN)
        )
        yield from (callable() for callable in map(random.choice, mapper))
