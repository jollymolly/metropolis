import typing
import unittest

import logic.helpers as helpers


class HelpersTestCase(unittest.TestCase):

    def test_route_instructions_generator(self):

        rig = helpers.RouteInstructionsGenerator()

        self.assertTrue(isinstance(rig, typing.Iterable))
        it = iter(rig)
        self.assertTrue(isinstance(it, typing.Generator))
        inst = next(it)
        self.assertTrue(isinstance(inst, str))
