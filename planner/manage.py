import click
import logic.handler as handler


@click.group()
def main():
    pass


@main.command()
def run():
    handler.Handler().run()


if __name__ == "__main__":
    main()
