"""Implement service command to create basic infrastructure - database/tables - or drop them."""

import click
import common.database.connector as db_connector


@click.group()
def main():
    pass

@main.command()
def create_database():
    db_connector.DataBaseConnector.create_database()

@main.command()
def drop_database():
    db_connector.DataBaseConnector.drop_database()

@main.command()
def create_tables():
    db_connector.DataBaseConnector.create_tables()


if __name__ == "__main__":
    main()
