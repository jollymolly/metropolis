
.EXPORT_ALL_VARIABLES:

PYTHONPATH=$(shell pwd)

setup: create_venv create_database create_tables

create_database:
	./venv/bin/python service.py create-database

drop_database:
	./venv/bin/python service.py drop-database

create_tables:
	./venv/bin/python service.py create-tables

create_venv:
	virtualenv -p /usr/bin/python3 venv
	./venv/bin/python -m pip install -r requirements.txt

drop_venv:
	rm -rf ./venv

run:
	./venv/bin/python uploader/manage.py run 2>&1 > uploader.log &
	sleep 2
	./venv/bin/python planner/manage.py run 2>&1 > planner.log &
	./venv/bin/python robots/manage.py run 2>&1 | tee robot.log 

test:
	PYTHONPATH=$(PYTHONPATH):$(shell pwd)/planner ./venv/bin/pytest planner/tests
	PYTHONPATH=$(PYTHONPATH):$(shell pwd)/uploader ./venv/bin/pytest uploader/tests
	PYTHONPATH=$(PYTHONPATH):$(shell pwd)/robots ./venv/bin/pytest robots/tests

flake:
	./venv/bin/flake8 --ignore=E501 common && ./venv/bin/flake8 --ignore=E501 planner && ./venv/bin/flake8 --ignore=E501 robots && ./venv/bin/flake8 --ignore=E501 uploader

clean: drop_database drop_venv


